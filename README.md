# Maquette SonoTec
Projet de démonstration pour l'interface de l'application SonoTec

## Installation
Télécharger le fichier .APK dans le dossier [release](https://gitlab.com/conception-electronique-prive/sonotec/maquette-app/-/releases)

Ouvrir l'APK depuis votre téléphone. Il est possible qu'Android demande l'autorisation d'installer des applications depuis votre utilitaire de fichiers. Veuillez lui donner alors cette permission.