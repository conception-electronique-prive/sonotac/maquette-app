package ca.doublenom.maquettesonotac

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.ui.graphics.Color
import androidx.navigation.findNavController

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val home = findViewById<ImageView>(R.id.toolbar_home)
        home.setOnClickListener {
            findNavController(R.id.nav_host_fragment).navigate(R.id.action_global_welcomeFragment)
        }

        val bluetooth = findViewById<ImageView>(R.id.toolbar_bluetooth)
        bluetooth.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setIcon(R.drawable.ic_baseline_bluetooth_24)
                .setTitle("Device discovery")
                .setMessage("Here, we will be able to discover new devices")
                .setNegativeButton(android.R.string.cancel) { _, _ ->  }
                .create()
                .show()
        }

    }

    fun onClickTrial(view: View) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://produits.neurospa.ca/"))
        startActivity(intent)
    }

}