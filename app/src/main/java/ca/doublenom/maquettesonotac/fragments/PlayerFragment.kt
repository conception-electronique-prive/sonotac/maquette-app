package ca.doublenom.maquettesonotac.fragments

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import ca.doublenom.maquettesonotac.R
import java.util.*

class PlayerFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.player_fragment, container, false)
        val program: ViewGroup = view.findViewById(R.id.player_fragment_title)
        val progressBar: ProgressBar = view.findViewById(R.id.player_fragment_progress_bar)
        val play: ImageView = view.findViewById(R.id.player_fragment_play_button)
        val pause: ImageView = view.findViewById(R.id.player_fragment_pause_button)
        var timer = Timer()

        val icon : ImageView = view.findViewById(R.id.player_fragment_icon)
        val ims = view.resources.assets.open("multimedia-audio-player-icon.png" )
        val d = Drawable.createFromStream(ims, null)
        icon.setImageDrawable(d)
        ims.close()

        progressBar.max = 500

        play.setOnClickListener {
            it.visibility = View.GONE
            pause.visibility = View.VISIBLE
            timer = Timer()
            timer.scheduleAtFixedRate(
                object : TimerTask() {
                    override fun run() {
                        progressBar.progress = progressBar.progress + 1
                        if(progressBar.progress == progressBar.max) {
                            progressBar.progress = 0
                            Handler(Looper.getMainLooper()).post {
                                pause.performClick()
                            }
                        }
                    }
                }, 0, 100
            )
        }

        pause.setOnClickListener {
            it.visibility = View.GONE
            play.visibility = View.VISIBLE
            timer.cancel()
            timer.purge()
        }

//        play.performClick()

        val title = program.findViewById<TextView>(R.id.program_item_title)
        val desc = program.findViewById<TextView>(R.id.program_item_description)
        val duration = program.findViewById<TextView>(R.id.program_item_duration)
        title.text = requireArguments().getString("title", "")
        desc.text = requireArguments().getString("description", "")
        duration.text = requireArguments().getString("duration", "")

        return view
    }
}