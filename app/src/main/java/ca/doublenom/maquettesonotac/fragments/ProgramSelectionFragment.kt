package ca.doublenom.maquettesonotac.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.compose.animation.core.snap
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import ca.doublenom.maquettesonotac.models.ProgramItem
import ca.doublenom.maquettesonotac.R
import ca.doublenom.maquettesonotac.models.CategoryProgramsItem
import ca.doublenom.maquettesonotac.models.CategoryItem

class ProgramSelectionFragment : Fragment() {
    class SideMenuAdapter(val dataset: ArrayList<CategoryItem>, private val cb: (Int) -> Unit) :
        RecyclerView.Adapter<SideMenuAdapter.ViewHolder>() {
        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val icon: ImageView = view.findViewById(R.id.program_selection_side_menu_item_icon)
            val title: TextView = view.findViewById(R.id.program_selection_side_menu_item_title)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                R.layout.category_item, parent, false
            )
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.icon.setImageDrawable(dataset[position].icon)
            holder.title.text = dataset[position].title
            holder.title.visibility = View.GONE
            holder.icon.setOnClickListener {
                cb(position)
            }
            if (dataset[position].selected)
                holder.itemView.setBackgroundResource(R.color.secondaryColor)
            else
                holder.itemView.setBackgroundResource(android.R.color.transparent)
        }

        override fun getItemCount() = dataset.size
    }

    class ProgramAdapter(private val dataset: Array<ProgramItem>) :
        RecyclerView.Adapter<ProgramAdapter.ViewHolder>() {
        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val title: TextView = view.findViewById(R.id.program_item_title)
            val description: TextView = view.findViewById(R.id.program_item_description)
            val duration: TextView = view.findViewById(R.id.program_item_duration)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                R.layout.program_item, parent, false
            )
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = dataset[position]
            holder.title.text = item.title
            holder.description.text = item.description
            holder.duration.text = item.duration
            holder.itemView.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("title", item.title)
                bundle.putString("description", item.description)
                bundle.putString("duration", item.duration)
                holder.itemView.findNavController()
                    .navigate(R.id.action_programSelectionFragment_to_playerFragment, bundle)
            }
        }

        override fun getItemCount() = dataset.size
    }

    class CategoryProgramsAdapter(private val dataset: Array<CategoryProgramsItem>) :
        RecyclerView.Adapter<CategoryProgramsAdapter.ViewHolder>() {
        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val title: TextView = view.findViewById(R.id.category_programs_item_title)
            val list: RecyclerView =
                view.findViewById(R.id.program_selection_category_programs_list)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                R.layout.category_programs_item, parent, false
            )
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val item = dataset[position]
            holder.title.text = item.title
            holder.title.visibility = View.GONE
            holder.list.adapter = ProgramAdapter(item.programs.toTypedArray())
        }

        override fun getItemCount() = dataset.size
    }

    @SuppressLint("ResourceType") // Linter being retarded on getDrawable ¯\_(ツ)_/¯
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val index = arguments?.getInt("index", 0) ?: 0
        val view = inflater.inflate(R.layout.program_selection_fragment, container, false)
        val categoryTitle: TextView = view.findViewById(R.id.program_selection_category_title)
        val programsList =
            view.findViewById<RecyclerView>(R.id.program_selection_category_programs_list)
        val sideMenuArray = ArrayList<CategoryItem>()
        val sideMenu = view.findViewById<RecyclerView>(R.id.program_selection_side_menu)


        val iconArray = resources.obtainTypedArray(R.array.program_category_icon)
        val titleArray = resources.getStringArray(R.array.program_category_title)
        val sideMenuAdapter = SideMenuAdapter(sideMenuArray) {
            for (e in sideMenuArray) {
                e.selected = false
            }
            sideMenuArray[it].selected = true
            sideMenu.adapter!!.notifyDataSetChanged()
            categoryTitle.text = sideMenuArray[it].title
            programsList.scrollToPosition(it)
        }
        for (i in titleArray.indices) {
            sideMenuArray.add(CategoryItem(iconArray.getDrawable(i)!!, titleArray[i]))
        }
        sideMenuArray[index].selected = true
        sideMenu.adapter = sideMenuAdapter
        iconArray.recycle()

        val categoryPrograms = ArrayList<CategoryProgramsItem>(titleArray.size)
        for (i in titleArray.indices) {
            categoryPrograms.add(CategoryProgramsItem(i, view.context))
        }
        programsList.adapter = CategoryProgramsAdapter(categoryPrograms.toTypedArray())
        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(programsList)
        programsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val view = snapHelper.findSnapView(recyclerView.layoutManager)
                val pos = recyclerView.layoutManager!!.getPosition(view!!)
                for (e in sideMenuArray) {
                    e.selected = false
                }
                sideMenuArray[pos].selected = true
                sideMenu.adapter!!.notifyDataSetChanged()
                categoryTitle.text = sideMenuArray[pos].title
            }
        })

        sideMenuArray[index].selected = true
        sideMenu.adapter!!.notifyDataSetChanged()
        categoryTitle.text = sideMenuArray[index].title
        programsList.scrollToPosition(index)


        return view
    }
}