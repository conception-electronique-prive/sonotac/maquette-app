package ca.doublenom.maquettesonotac.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import ca.doublenom.maquettesonotac.R
import ca.doublenom.maquettesonotac.models.CategoryItem

class CategorySelectionFragment : Fragment() {
    class CategoryAdapter(val dataset: ArrayList<CategoryItem>, private val cb: (Int) -> Unit) :
        RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val icon: ImageView = view.findViewById(R.id.program_selection_side_menu_item_icon)
            val title: TextView = view.findViewById(R.id.program_selection_side_menu_item_title)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                R.layout.category_item, parent, false
            )
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.icon.setImageDrawable(dataset[position].icon)
            holder.title.text = dataset[position].title
            holder.title.visibility = View.VISIBLE
            holder.itemView.setOnClickListener {
                cb(position)
            }
        }

        override fun getItemCount() = dataset.size
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.category_selection_fragment, container, false)
        val categoryList = view.findViewById<RecyclerView>(R.id.category_selection_list)
        val iconArray = resources.obtainTypedArray(R.array.program_category_icon)
        val titleArray = resources.getStringArray(R.array.program_category_title)
        val categoryArray = ArrayList<CategoryItem>()
        val categoryAdapter = CategoryAdapter(categoryArray) {
            val bundle = Bundle()
            bundle.putInt("index", it)
            view.findNavController()
                .navigate(
                    R.id.action_categorySelectionFragment_to_programSelectionFragment,
                    bundle
                )
        }
        for (i in titleArray.indices) {
            categoryAdapter.dataset.add(CategoryItem(iconArray.getDrawable(i)!!, titleArray[i]))
        }
        categoryList.adapter = categoryAdapter
        iconArray.recycle()
        return view
    }

}