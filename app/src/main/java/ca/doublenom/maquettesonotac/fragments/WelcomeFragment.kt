package ca.doublenom.maquettesonotac.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ca.doublenom.maquettesonotac.R

class WelcomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.welcome_fragment, container, false)
        val tvButton = view.findViewById<TextView>(R.id.welcome_fragment_category_selection)
        tvButton.setOnClickListener {
            findNavController().navigate(R.id.action_welcomeFragment_to_categorySelectionFragment)
        }
        return view
    }
}