package ca.doublenom.maquettesonotac

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import ca.doublenom.maquettesonotac.models.ProgramItem

class ProgramSelectionCurrentCategoryFragment(private val index: Int) : Fragment() {
    private lateinit var tvTitle: TextView
    private lateinit var rvPrograms: RecyclerView

    class ProgramAdapter(private val dataset: Array<ProgramItem>) :
        RecyclerView.Adapter<ProgramAdapter.ViewHolder>() {
        class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val title: TextView = view.findViewById(R.id.program_item_title)
            val description: TextView = view.findViewById(R.id.program_item_description)
            val duration: TextView = view.findViewById(R.id.program_item_duration)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(
                R.layout.program_item, parent, false
            )
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.title.text = dataset[position].title
            holder.description.text = dataset[position].description
            holder.duration.text = dataset[position].duration
        }

        override fun getItemCount() = dataset.size
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view =
            inflater.inflate(R.layout.category_programs_item, container, false)
        tvTitle = view.findViewById(R.id.category_programs_item_title)
        rvPrograms = view.findViewById(R.id.program_selection_category_programs_list)

        val categoryArray = resources.getStringArray(R.array.program_category_title)
        val titleArray = resources.obtainTypedArray(R.array.program_content_title)
        val descArray = resources.obtainTypedArray(R.array.program_content_description)
        val titleId = titleArray.getResourceId(0, R.array.program_energy_boost_content_title)
        val descId = descArray.getResourceId(0, R.array.program_energy_boost_content_description)
        val titles = resources.getStringArray(titleId)
        val descriptions = resources.getStringArray(descId)
        val programs = ArrayList<ProgramItem>(titles.size)
        for (i in titles.indices) {
            programs.add(ProgramItem(titles[i], descriptions[i], "0 minutes"))
        }
        tvTitle.text = categoryArray[0]
        rvPrograms.adapter = ProgramAdapter(programs.toTypedArray())

        titleArray.recycle()
        descArray.recycle()

        return view

    }
}