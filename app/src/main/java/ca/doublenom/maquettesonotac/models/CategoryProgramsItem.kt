package ca.doublenom.maquettesonotac.models

import android.content.Context
import ca.doublenom.maquettesonotac.R

class CategoryProgramsItem(index: Int, context: Context) {
    val title: String
    val programs: ArrayList<ProgramItem>

    init {
        val categoryTitles = context.resources.getStringArray(R.array.program_category_title)
        val categoryContentTitleArrays =
            context.resources.obtainTypedArray(R.array.program_content_title)
        val categoryContentDescArrays =
            context.resources.obtainTypedArray(R.array.program_content_description)
        val categoryContentDurationArrays =
                context.resources.obtainTypedArray(R.array.program_content_duration)

        val contentTitles = categoryContentTitleArrays.getTextArray(index)
        val contentDescriptions = categoryContentDescArrays.getTextArray(index)
        val contentDurations = categoryContentDurationArrays.getTextArray(index)

        title = categoryTitles[index]
        programs = ArrayList(contentTitles.size)
        for (i in contentTitles.indices) {
            programs.add(
                ProgramItem(
                    contentTitles[i].toString(),
                    contentDescriptions[i].toString(),
                    contentDurations[i].toString()
                )
            )
        }

        categoryContentTitleArrays.recycle()
        categoryContentDescArrays.recycle()
        categoryContentDurationArrays.recycle()
    }
}