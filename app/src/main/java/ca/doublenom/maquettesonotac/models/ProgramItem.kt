package ca.doublenom.maquettesonotac.models

data class ProgramItem(val title: String, val description: String, val duration: String)
