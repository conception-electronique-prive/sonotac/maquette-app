package ca.doublenom.maquettesonotac.models

import android.graphics.drawable.Drawable

data class CategoryItem(val icon: Drawable, val title: String, var selected: Boolean = false)
